const express = require('express');
const router = express.Router();

// declare axios for making http requests
const axios = require('axios');
const API = 'https://jsonplaceholder.typicode.com';


//FIREBASE

var firebase = require("firebase");

firebase.initializeApp({
  serviceAccount:"./CupidTravel-b7604d942227.json",
  databaseURL:"https://cupidtravel-7604c.firebaseio.com/"
});
var ref = firebase.database().ref('users/');
// var messageRef =  ref.child('messages');
// messageRef.push({
//   name:"Test"
// });





//FIREBASE END

/* GET api listing. */
router.get('/', (req, res) => {
  res.send('api works');
});

// Get all posts
// router.get('/posts', (req, res) => {
//   // Get posts from the mock api
//   // This should ideally be replaced with a service that connects to MongoDB
//   axios.get(`${API}/posts`)
//     .then(posts => {
//       res.status(200).json(posts.data);
//     })
//     .catch(error => {
//       res.status(500).send(error)
//     });
// });
router.get('/users', (req, res) => {
  // Get posts from the mock api
  // This should ideally be replaced with a service that connects to MongoDB

  ref.once("value", function(snapshot) {
    res.send(snapshot);
    console.log(snapshot.val());  //call the .val() here
  }, function (errorObject) {
   console.log("The read failed: " + errorObject.code);
  });

});

module.exports = router;
