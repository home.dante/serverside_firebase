import { Component, OnInit } from '@angular/core';
import { PostsService } from '../posts.service';
@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.css']
})
export class PostsComponent implements OnInit {

  posts: any = [];
  users:any=[];
 constructor(private postsService: PostsService) { }

 ngOnInit() {
   // Retrieve posts from the API
  //  this.postsService.getAllPosts().subscribe(posts => {
  //    this.posts = posts;
  //  });
  this.postsService.getAllUsers().subscribe(users => {
    this.users = users;
    console.log(users);
    console.log("logging");
  
  });
 }

}
